#!/usr/bin/env python
# coding: utf-8

# # Indian State wise Covid-19 Exploratory Analysis
# #By- Aarush Kumar
# #Dated: July 03,2021

# In[1]:


import numpy as np 
import pandas as pd 
import matplotlib.pyplot as plt
get_ipython().run_line_magic('matplotlib', 'inline')
import seaborn as sns


# In[2]:


file = '/home/aarush100616/Downloads/Projects/Indian State wise Covid-19 Analysis/Latest Covid-19 India Status.csv'
df = pd.read_csv(file)


# In[3]:


df


# In[4]:


df.columns


# In[5]:


df.shape


# In[6]:


df.isna().sum()


# In[7]:


df['State/UTs']


# In[8]:


plt.style.use('seaborn')
plt.figure(figsize=(10,10))
plt.title('Total Cases')
sns.barplot(data=df,x='Total Cases',y='State/UTs',color='b')
plt.show()


# In[9]:


plt.style.use('seaborn')
plt.figure(figsize=(10,10))
plt.title('Active Cases')
sns.barplot(data=df,x='Active',y='State/UTs',color='y')
plt.show()


# In[10]:


plt.style.use('seaborn')
plt.figure(figsize=(10,10))
plt.title('Discharged')
sns.barplot(data=df,x='Discharged',y='State/UTs',color='pink')
plt.show()


# In[11]:


plt.style.use('seaborn')
plt.figure(figsize=(10,10))
plt.title('Deaths')
sns.barplot(data=df,x='Deaths',y='State/UTs',color='r')
plt.show()


# In[12]:


for col in df.columns[1:5]:
    print(f'Highest {col}')
    print(df[['State/UTs',col]][df[col] == df[col].max()])
    print('\n')


# In[13]:


for col in df.columns[1:5]:
    print(f'Least {col}')
    print(df[['State/UTs',col]][df[col] == df[col].min()])
    print('\n')


# In[14]:


colors = ['#2ecc71','#3498db','#e74c3c']
labels = ['Active','Discharged','Deaths']


# In[15]:


for i in range(36):
    plt.style.use('fivethirtyeight')
    plt.figure(figsize=(5,5))
    plt.title(df.loc[i]['State/UTs'])
    plt.pie([df.loc[i]['Active'],df.loc[i]['Discharged'],df.loc[i]['Deaths']],colors=colors,labels=labels,explode=[0,0,0.2],autopct='%0.1f')
    plt.show()

